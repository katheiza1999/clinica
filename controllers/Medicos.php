<?php

  class Medicos extends CI_Controller
  {

    function __construct()
    {
       parent::__construct();
       //cargar modelo
       $this->load->model('Medico');
    }
    //funciones que reenderiza a vista index
    public function index(){
      $data['medicos']=$this->Medico->obtenerTodos();
      $this->load->view('header');
      $this->load->view('medicos/index',$data);
      $this->load->view('footer');
    }
    //funciones que reenderiza a vista nuevo
    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('medicos/nuevo');
      $this->load->view('footer');
    }
    public function guardar(){
      $datosNuevoMedico=array(
        //convocamos al modelo
        "cedula_med"=>$this->input->post('cedula_med'),
        "primer_apellido_med"=>$this->input->post('primer_apellido_med'),
        "segundo_apellido_med"=>$this->input->post('segundo_apellido_med'),
        "nombres_med"=>$this->input->post('nombres_med'),
        "especialidad_med"=>$this->input->post('especialidad_med'),
        "direccion_med"=>$this->input->post('direccion_med')
      );
      if ($this->Medico->insertar($datosNuevoMedico)) {
        redirect('medicos/index'); //aqui poner le nombre de la funcion

      }else{
         echo "<h1>ERROR AL INSEERTAR</h1>";
      }
    }
    //funcion para elminar medicos
    public function eliminar($id_med){
      if ($this->Medico->borrar($id_med)) {
        redirect('medicos/index');
      }else {
        echo "ERROR AL BORRAR :(";
      }
    }
  }//cierre de clase


 ?>
