<?php

  class Pacientes extends CI_Controller
  {

    function __construct()
    {
       parent::__construct();
       //cargar modelo
       $this->load->model('Paciente');
    }
    //funciones que reenderiza a vista index
    public function index(){
      $data['pacientes']=$this->Paciente->obtenerTodos();
      $this->load->view('header');
      $this->load->view('pacientes/index',$data);
      $this->load->view('footer');
    }
    //funciones que reenderiza a vista nuevo
    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('pacientes/nuevo');
      $this->load->view('footer');
    }
    public function guardar(){
      $datosNuevoPaciente=array(
        //convocamos al modelo
        "cedula_pas"=>$this->input->post('cedula_pas'),
        "primer_apellido_pas"=>$this->input->post('primer_apellido_pas'),
        "segundo_apellido_pas"=>$this->input->post('segundo_apellido_pas'),
        "nombres_pas"=>$this->input->post('nombres_pas'),
        "direccion_pas"=>$this->input->post('direccion_pas'),
        "enfermedad_pas"=>$this->input->post('enfermedad_pas'),
      );
      if ($this->Paciente->insertar($datosNuevoPaciente)) {
        redirect('pacientes/index'); //aqui poner le nombre de la funcion

      }else{
         echo "<h1>ERROR AL INSEERTAR</h1>";
      }
    }
    //funcion para elminar pacientes
    public function eliminar($id_pas){
      if ($this->Paciente->borrar($id_pas)) {
        redirect('pacientes/index');
      }else {
        echo "ERROR AL BORRAR :(";
      }
    }
  }//cierre de clase

 ?>
