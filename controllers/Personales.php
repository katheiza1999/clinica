<?php

  class Personales extends CI_Controller
  {

    function __construct()
    {
       parent::__construct();
       //cargar modelo
       $this->load->model('Personal');
    }
    //funciones que reenderiza a vista index
    public function index(){
      $data['personales']=$this->Personal->obtenerTodos();
      $this->load->view('header');
      $this->load->view('personales/index',$data);
      $this->load->view('footer');
    }
    //funciones que reenderiza a vista nuevo
    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('personales/nuevo');
      $this->load->view('footer');
    }
    public function guardar(){
      $datosNuevoPersonal=array(
        //convocamos al modelo
        "cedula_per"=>$this->input->post('cedula_per'),
        "primer_apellido_per"=>$this->input->post('primer_apellido_per'),
        "segundo_apellido_per"=>$this->input->post('segundo_apellido_per'),
        "nombres_per"=>$this->input->post('nombres_per'),
        "cargo_per"=>$this->input->post('cargo_per'),
        "direccion_per"=>$this->input->post('direccion_per')
      );
      if ($this->Personal->insertar($datosNuevoPersonal)) {
        redirect('personales/index'); //aqui poner le nombre de la funcion

      }else{
         echo "<h1>ERROR AL INSEERTAR</h1>";
      }
    }
    //funcion para elminar personal
    public function eliminar($id_per){
      if ($this->Personal->borrar($id_per)) {
        redirect('personales/index');
      }else {
        echo "ERROR AL BORRAR :(";
      }
    }
  }//cierre de clase


 ?>
