<?php
  class Medico extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un medico en MYSQL
    function insertar($datos){
        return $this->db->insert("medico",$datos);
    }
    //funcion para consultar medicos
    public function obtenerTodos(){
      $listadoMedicos=$this->db->get("medico");
      if ($listadoMedicos->num_rows()>0) {  //para saber si hay datos o no hay datos
        return $listadoMedicos->result();
      }else{ //no hay datos
        return false;

      }
    }
    //borrar medico
    function borrar($id_med){
      $this->db->where("id_med",$id_med);
      if ($this->db->delete("medico")) {
        return true;
      }else {
        return false;
      }
    }

  }//Cierre de la clase

 ?>
