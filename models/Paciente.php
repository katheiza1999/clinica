<?php
  class Paciente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un paciente en MYSQL
    function insertar($datos){
        return $this->db->insert("paciente",$datos);
    }
    //funcion para consultar paciente
    public function obtenerTodos(){
      $listadoPacientes=$this->db->get("paciente");
      if ($listadoPacientes->num_rows()>0) {  //para saber si hay datos o no hay datos
        return $listadoPacientes->result();
      }else{ //no hay datos
        return false;

      }
    }
    //borrar paciente
    function borrar($id_pas){
      $this->db->where("id_pas",$id_pas);
      if ($this->db->delete("paciente")) {
        return true;
      }else {
        return false;
      }
    }

  }//Cierre de la clase

 ?>
