<?php
  class Personal extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un personal en MYSQL
    function insertar($datos){
        return $this->db->insert("personal",$datos);
    }
    //funcion para consultar personal
    public function obtenerTodos(){
      $listadoPersonales=$this->db->get("personal");
      if ($listadoPersonales->num_rows()>0) {  //para saber si hay datos o no hay datos
        return $listadoPersonales->result();
      }else{ //no hay datos
        return false;

      }
    }
    //borrar 
    function borrar($id_per){
      $this->db->where("id_per",$id_per);
      if ($this->db->delete("personal")) {
        return true;
      }else {
        return false;
      }
    }

  }//Cierre de la clase

 ?>
