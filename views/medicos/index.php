<h1 class="text-center"><b>LISTADO DE MEDICOS</b></h1>
<br>
<?php if ($medicos): ?>
    <table class="table table-striped
    table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>PRIMER APELLIDO</th>
             <th>SEGUNDO APELLIDO</th>
             <th>NOMBRES</th>
             <th>ESPECIALIDAD</th>
             <th>TELEFONO</th>
             <th>DIRECCION</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($medicos as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_med; ?>
              </td>
              <td>
                <?php echo $filaTemporal->cedula_med; ?>
              </td>
              <td>
                <?php echo $filaTemporal->primer_apellido_med; ?>
              </td>
              <td>
                <?php echo $filaTemporal->segundo_apellido_med; ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombres_med; ?>
              </td>
              <td>
                <?php echo $filaTemporal->especialidad_med; ?>
              </td>
              <td>
                <?php echo $filaTemporal->telefono_med; ?>
              </td>
              <td>
                <?php echo $filaTemporal->direccion_med; ?>
              </td>
                  <td class="text-center">
                  <a href="#" title="Editar Medico">
                    <i class="glyphicon glyphicon-pencil"></i>
                </a>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url();?>/medicos/eliminar/<?php echo $filaTemporal->id_med;?>" title="Eliminar Medico" style="color:red;">
                <i class="glyphicon glyphicon-trash"></i>
                </a>
              </td>

            </tr>

          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1>No hay medicos</h1>
<?php endif; ?>
