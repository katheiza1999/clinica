<h1 class="text-center"><b>LISTADO DE PACIENTES</b></h1>
<br>
<?php if ($pacientes): ?>
    <table class="table table-striped
    table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>PRIMER APELLIDO</th>
             <th>SEGUNDO APELLIDO</th>
             <th>NOMBRES</th>
             <th>TELEFONO</th>
             <th>DIRECCION</th>
             <th>ENFERMEDAD</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($pacientes as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_pas; ?>
              </td>
              <td>
                <?php echo $filaTemporal->cedula_pas; ?>
              </td>
              <td>
                <?php echo $filaTemporal->primer_apellido_pas; ?>
              </td>
              <td>
                <?php echo $filaTemporal->segundo_apellido_pas; ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombres_pas; ?>
              </td>
              <td>
                <?php echo $filaTemporal->telefono_pas; ?>
              </td>
              <td>
                <?php echo $filaTemporal->direccion_pas; ?>
              </td>
              <td>
                <?php echo $filaTemporal->enfermedad_pas; ?>
              </td>
                  <td class="text-center">
                  <a href="#" title="Editar Paciente">
                    <i class="glyphicon glyphicon-pencil"></i>
                </a>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url();?>/pacientes/eliminar/<?php echo $filaTemporal->id_pas;?>" title="Eliminar Pacientes" style="color:red;">
                <i class="glyphicon glyphicon-trash"></i>
                </a>
              </td>

            </tr>

          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1>No hay pacientes</h1>
<?php endif; ?>
