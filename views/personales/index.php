<h1 class="text-center"><b>LISTADO DE PERSONAL ADMINISTRATIVO</b></h1>
<br>
<?php if ($personales): ?>
    <table class="table table-striped
    table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>PRIMER APELLIDO</th>
             <th>SEGUNDO APELLIDO</th>
             <th>NOMBRES</th>
             <th>CARGO</th>
             <th>TELEFONO</th>
             <th>DIRECCION</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($personal as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_per; ?>
              </td>
              <td>
                <?php echo $filaTemporal->cedula_per; ?>
              </td>
              <td>
                <?php echo $filaTemporal->primer_apellido_per; ?>
              </td>
              <td>
                <?php echo $filaTemporal->segundo_apellido_per; ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombres_per; ?>
              </td>
              <td>
                <?php echo $filaTemporal->cargo_per; ?>
              </td>
              <td>
                <?php echo $filaTemporal->telefono_per; ?>
              </td>
              <td>
                <?php echo $filaTemporal->direccion_per; ?>
              </td>
                  <td class="text-center">
                  <a href="#" title="Editar Personal">
                    <i class="glyphicon glyphicon-pencil"></i>
                </a>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url();?>/personales/eliminar/<?php echo $filaTemporal->id_med;?>" title="Eliminar personal" style="color:red;">
                <i class="glyphicon glyphicon-trash"></i>
                </a>
              </td>

            </tr>

          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1>No hay personal</h1>
<?php endif; ?>
